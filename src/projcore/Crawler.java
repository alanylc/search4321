package projcore;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Crawler crawls a page and extract stuffs and put them into PAGEDB. Words extracted are passed 
 * to Index class for reverse indexing.
 * @author hin
 *
 */
public class Crawler implements Serializable 
{
	private static final long serialVersionUID = 1L;
	static LinkedList<String> urls=new LinkedList<String>();
	static Vector<String> urlsDone=new Vector<String>();
	static Vector<Crawler> crawlerResults=new Vector<Crawler>();
	static String scope;
	static PageDatabase  db;
	static int TTL;
	public String url;
	public String title;
	public int size;
	public Date lastMod;
	public Vector<Posting> words;
	public Vector<String> childLink;
	public Vector<String> parentLink= new Vector<String>();
	Crawler(String _url)
	{

		url = _url;
	}
	/**
	 * It first extract words from webpage, then it eliminate stopwords and find the stem of the remaining words.
	 * Words are sorted in order to find the frequency of words.
	 * @param doc Webpage
	 * @return An array of (word frequency) pair
	 * @throws ParserException
	 */
	public Vector<Posting> extractWords(Document doc) throws Exception

	{
		Vector<String> v_word = new Vector<String>();
        StringTokenizer st = new StringTokenizer(doc.select("html").text().replaceAll(String.valueOf((char) 160), " "));
		StopStem stopStem = new StopStem("stopwords.txt");
        while (st.hasMoreTokens()) {
        	String nextWord=st.nextToken();
        	if (!stopStem.isStopWord(nextWord)){
        		nextWord=stopStem.stem(nextWord);
        		if(!nextWord.equals("") && nextWord!=null && !nextWord.equals(" "))
        			v_word.add(nextWord);
        	}
        }
        
		Vector<Posting> words=new Vector<Posting>();
		String lastWord="";
		Comparator<String> comparatorS = new Comparator<String>() {
		    public int compare(String c1, String c2) {
		        return c1.compareTo(c2);
		    }
		};
		Collections.sort(v_word,comparatorS);
		System.out.println(v_word);
		int frequency=0;
		for(String word:v_word){
			if(lastWord.equals(word)){
				frequency++;
			}
			else{
				words.add(new Posting(lastWord,frequency));
				lastWord=word;
				frequency=1;
			}
		}
		Comparator<Posting> comparatorP = new Comparator<Posting>() {
		    public int compare(Posting c1, Posting c2) {
		        return c2.freq-c1.freq;
		    }
		};
		Collections.sort(words,comparatorP);
        return words;
			
	}
	/**
	 * Extract and return all links in a webpage.
	 * @param doc Webpage
	 * @return An array of links extracted.
	 * @throws ParserException
	 */
	public Vector<String> extractLinks(Document doc) throws Exception

	{
		Vector<String> v_link=new Vector<String>();
		Elements links = doc.select("a[href]");
		for (Element link : links) {
			v_link.add(link.attr("abs:href"));
        }
	    return v_link;
	}
	/**
	 * Extract and return the title of webpage.
	 * @param doc Webpage.
	 * @return String containing the title.
	 */
	private String extractTitle(Document doc) {
		return doc.select("title").text();
	}
	/**
	 * Extract and return the content length of webpage from response header.
	 * @param response HTML response.
	 * @return integer conmtaining size.
	 */
	private int extractSize(Response response) {
		try{
			return Integer.parseInt(response.header("Content-Length"));
		}catch(Exception e){
		}
		return response.body().length();
	}
	/**
	 * Extract and return the last modified date of webpage. If not found, 'Date' would be used instead. 
	 * If not found either, Current time would be returned.
	 * @param response HTML response.
	 * @return Date extracted.
	 */
	private Date extractLastMod(Response response) {
		String dateString = response.header("Last-Modified");
		SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
		try {
			Date d = format.parse(dateString);
			return d;
		} catch (Exception e) {
		}
		try {
			dateString = response.header("Date");
			Date d = format.parse(dateString);
			return d;
		} catch (Exception e) {
		}
		return new Date();
	}
	/**
	 * Main function of crawler.
	 * @param url First URL to be crawled.
	 * @param _scope URL to be crawled must include this string, if not, the page will not be crawled.
	 * @param TTL number of page need to get
	 */
	public static void main (String url,String _scope,int _TTL)
	{
		TTL=_TTL;
		try
		{
			db=new PageDatabase(jdbm.PAGEDB,jdbm.PAGEDB);
			scope=_scope;
			urls.add(url);
			while(urls.peek()!=null&&TTL!=0){
				String urlCurrent=urls.remove();
				if(!done(urlCurrent)){
					extract(new Crawler(urlCurrent));
					TTL--;
				}
			}
			for(Crawler c:crawlerResults){		//add ParentLink
				for(int i = 0; i < c.childLink.size(); i++){
					if(inscope(c.childLink.get(i))){
						try {
							int id=Maps.getPageID(c.childLink.get(i).toString());
							Crawler temp=(Crawler) db.get(id);
							temp.parentLink.add(c.url);
							db.add(id, temp,false);
						} catch (Exception e) {
						}
					}
				}
			}
			db.finalize();
		}
		catch (Exception e)
            	{
                	e.printStackTrace ();
            	}

	}
	/**
	 * Compare the LastMod in newly crawled page and the one in database to see if the page is modified.
	 * @param crawler current crawled page.
	 * @param id 
	 * @return True if lastMod in newly crawled page is later than the one in database.
	 */
	private static boolean modified(Crawler crawler, int id) {
		try{
			Crawler temp=(Crawler) db.get(id);
			Date lastMod=temp.lastMod;
			if(crawler.lastMod.getTime()-lastMod.getTime()<=0)//"crawler.lastMod is not after lastMod in db"
				return false;
		}catch(Exception e){
		}
		return true;
	}
	/**
	 * To check if the URL is in scope.
	 * @param urlCurrent URL to be crawled.
	 * @return True if in scope.
	 */
	private static boolean inscope(String urlCurrent) {
		if(urlCurrent.contains(scope))
			return true;
		return false;
	}
	/**
	 * Ckeck if the page has just been parsed.
	 * @param url to be crawled.
	 * @return True if the page had just been parsed.
	 */
	private static boolean done(String url) {
		for (String urlDone : urlsDone)
			if (urlDone.equals(url))
				return true;
		return false;
	}
	/**
	 * Extract a page and store in database.
	 * @param crawler To store the page.
	 * @throws ParserException
	 * @throws IOException
	 */
	private static void extract(Crawler crawler) throws Exception {
		try {
			Response response= Jsoup.connect(crawler.url).timeout(6000) .execute();
			Document doc = response.parse();
			crawler.lastMod=crawler.extractLastMod(response);
			crawler.childLink = crawler.extractLinks(doc);
			for(int i = 0; i < crawler.childLink.size(); i++){
				if(inscope(crawler.childLink.get(i))&& !urls.contains(crawler.childLink.get(i)))
					urls.add(crawler.childLink.get(i));
			}
			urlsDone.add(crawler.url);
			int id=Maps.getPageID(crawler.url);
			if(!modified(crawler,id)){
				System.out.println(crawler.url+" not modified.");
				return;
			}
			Crawler previousResult=(Crawler) db.get(id);
			// remove previous data
			if(previousResult!=null){
					Indexer.remove(previousResult.words, id);
				db.delEntry(id);
			}
			// read new data
			
			crawler.title=crawler.extractTitle(doc);
			crawler.size=crawler.extractSize(response);
			crawler.words = crawler.extractWords(doc);
			Indexer.index(crawler.words,id);
			System.out.println(crawler.url+"\t\tQueue size="+urls.size());
			db.add(id,crawler,true);
			Maps.commit();
			crawlerResults.add(crawler);
		} catch (IOException e) {
			System.out.println("IO exception occured, "+crawler.url+" give up.");
			TTL++;
		}
	}
	@Override
	public String toString() {
		return title+"\r\n"+url+"\r\n"+lastMod+", "+size+"\r\n"+words+"\r\n"+VTS(childLink,"\r\n")+"---------------------------------------------------------------------------------------------";
	}
	/**
	 * Printing vector<String> with a seperator for each element.
	 * @param vs vector<String>
	 * @param seperator to seperate each element
	 * @return
	 */
	private String VTS(Vector<String> vs,String seperator) {
		// TODO Auto-generated method stub
		String r="";
		for(String s:vs)r+=(s+seperator);
		return r;
	}
	
}