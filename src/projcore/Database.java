package projcore;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import jdbm.RecordManager;
import jdbm.RecordManagerFactory;
import jdbm.helper.FastIterator;
import jdbm.htree.HTree;

/**
 * Database class for databases and maps.
 * @author hin
 *
 */
public class Database {
	protected RecordManager recman;
	protected HTree hashtable;
/**
 * Constructs a database.
 * @param recordmanager Filename.
 * @param objectname Object name.
 * @throws IOException
 */
	Database(String recordmanager, String objectname) throws IOException {
		 recman = RecordManagerFactory.createRecordManager(recordmanager);
		long recid = recman.getNamedObject(objectname);

		if (recid != 0) // If the object has already been recorded in record
						// manager;
			hashtable = HTree.load(recman, recid);

		else // If not, create a new hashtable;
		{
			hashtable = HTree.createInstance(recman);
			recman.setNamedObject(objectname, hashtable.getRecid());
		}
	}
	/**
	 * Closes a database.
	 * @throws IOException
	 */
	public void finalize() throws IOException {
		recman.commit();
		recman.close();
	}
	/**
	 * Commits when a database has been modified and need to save the changes to the database.
	 * @throws IOException
	 */
	public void commit() throws IOException {
		recman.commit();
	}
	/**
	 * Remove an entry from a database
	 * @param word Word to be removed.
	 * @throws IOException
	 */
	public void delEntry(String word) throws IOException {
		hashtable.remove(word);
	}
	/**
	 * Prints all entries in a database with string as key.
	 * @throws IOException
	 */
	public void printAll() throws IOException {
		//TODO change to print to a txt file.
		FastIterator iter = hashtable.keys();
		String key;
		while ((key = (String) iter.next()) != null) {
			System.out.println(key + ": " + hashtable.get(key));
		}
	}
	/**
	 * Prints all entries in a database with int as key.
	 * @throws IOException
	 */
	public void iprintAll(boolean writeToFile) throws IOException {
		removeLog();//remove log first
		FastIterator iter = hashtable.keys();
		int key;
		while (true) {
			try{
				key = (Integer) iter.next();
				if(!writeToFile)
					System.out.println(key + ": " + hashtable.get(key));
				else
					log(hashtable.get(key));
			}catch(Exception e){
				break;
			}
		}
	}
	/**
	 * Conut the number of entries of a database.
	 * @return Number of entries.
	 * @throws IOException
	 */
	public int countEntry() throws IOException {
		FastIterator iter = hashtable.keys();
		int count=0;
		while (iter.next() != null) {
			count++;
		}
		return count;
	}
	/**
	 * Get an object with key provided
	 * @param s String key provided
	 * @return Object Found.
	 * @throws IOException
	 */
	public Object get(String s) throws IOException{
		return hashtable.get(s);
	}
	/**
	 * Add an object to the database.
	 * @param s String key provided.
	 * @param o Object to be stored.
	 * @throws IOException
	 */
	public void add(String s, Object o) throws IOException {
		hashtable.put(s,o);
	}
	
	public void log(Object entry){
		try{
		Writer output;
		System.out.println(entry);
		output = new BufferedWriter(new FileWriter("spider_result.txt",true));
		output.append(entry+"\r\n");
		output.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}	
	/**
	 * Removing the log at the start of the program.
	 */
	public void removeLog() {
    	try{
    		 
    		File file = new File("spider_result.txt");
 
    		file.delete();
 
    	}catch(Exception e){
 
    	}
		
	}

}
