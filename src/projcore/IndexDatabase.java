package projcore;


import java.io.IOException;
import java.util.Vector;

/**
 * Database for storing word indexes.Add function overriden.
 * Key:WordID
 * Data:Vector<Posting> Posting=(Docid,freq)
 * @author hin
 *
 */
public class IndexDatabase extends Database{
	public IndexDatabase(String recordmanager, String objectname) throws IOException {
		super(recordmanager, objectname);
	}
	public Vector<Posting> getPostingList(int word_id) {
		try {
			return (Vector<Posting>) hashtable.get(word_id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public Vector<Posting> getPostingList(String word) {
		try {
			int word_id = Maps.getWordID(word);
			return (Vector<Posting>) hashtable.get(word_id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	/**
	 * Add the PageID frequency pair to appropriate entry in word database by binary insertion.
	 * @param p
	 * @param pid
	 */
	public void add(Posting p, int pid) {
		Vector<Posting> vp=new Vector<Posting>();
		try {
			int index=Maps.getWordID(p.word);
			if (hashtable.get(index)!= null) {
				vp = (Vector<Posting>) hashtable.get(index);
			}
			//Binary insert
			int key=p.freq;
			int mid=0, left = 0 ;
			int right =vp.size(); // one position passed the right end
			while (left < right) {
				mid = left + (right - left)/2;
				if (key < vp.elementAt(mid).freq){
				          left = mid+1;
				      }
				      else if (key > vp.elementAt(mid).freq){                                        
				        right = mid;
				      }
				      else {                                                                  
				        break;
				     }                                                                                                               
				   }
 
//			for(int i=0;i<vp.size();i++){
//				if(vp.elementAt(i).freq>p.freq)break;
//			}			
			try{
				if(vp.elementAt(mid).freq>key)mid+=1;
			}catch(Exception e){
			
			}
			vp.add(mid,new Posting("Doc"+pid,p.freq));
			hashtable.put(index, vp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void remove(Posting p, int pid) {
		Vector<Posting> vp=new Vector<Posting>();
		int index=0;
		try {
			index=Maps.getWordID(p.word);
			if (hashtable.get(index)!= null) {
				vp = (Vector<Posting>) hashtable.get(index);
			}
			//Binary insert
			int key=p.freq;
			int mid=0, left = 0 ;
			int right =vp.size(); // one position passed the right end
			while (left < right) {
				mid = left + (right - left)/2;
				if (key < vp.elementAt(mid).freq){
				          left = mid+1;
			      }
			      else if (key > vp.elementAt(mid).freq){                                        
			        right = mid;
			      }
			      else {                                                                  
			        break;
			     }                                                                                                               
			}

			int pt = mid;
			boolean deleted = false;
			while(pt>=0&&pt<vp.size()&&vp.elementAt(pt).freq==key) {
				// check if the posting is belong to the page
				if (vp.elementAt(pt).word.equals("Doc"+String.valueOf(pid))) {
					// remove posting
					vp.remove(pt);
					deleted = true;
					break;
				}
				pt -= 1;
			}
			pt = mid+1;
			// if not found in the left half, pt == -1, continue to find the the right half
			while(!deleted &&pt>=0&&pt<vp.size()&& vp.elementAt(pt).freq==key) {
				// check if the posting is belong to the page
				if (vp.elementAt(pt).word.equals("Doc"+String.valueOf(pid))) {
					// remove posting
					vp.remove(pt);
					break;
				}
				pt += 1;
			}
			hashtable.remove(index);
			hashtable.put(index, vp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
