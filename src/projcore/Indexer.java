package projcore;


import java.io.IOException;
import java.util.Vector;

/**
 * Indexer class for putting PageID frequency pair to appropriate entry in word database.
 * @author hin
 *
 */
public class Indexer {

	static IndexDatabase  db;

	public static void index(Vector<Posting> words, int pid) {
		try {
			db=new IndexDatabase(jdbm.WORDDB,jdbm.WORDDB);
			for(Posting p:words){
				db.add(p,pid);
			}
			db.finalize();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void remove(Vector<Posting> words, int pid) {
		try {
			db=new IndexDatabase(jdbm.WORDDB,jdbm.WORDDB);
			for(Posting p:words)
				db.remove(p, pid);
			db.finalize();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
