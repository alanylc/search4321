package projcore;


import java.io.IOException;

/**
 * Container and functions for word map and page map which maps words and pages to their specific ID.
 * @author hin
 *
 */
public class Maps {

	private static int countWord;
	private static int countPage;
	private static boolean countedWord=false;
	private static boolean countedPage=false;
	private static Database  wordmap;
	private static Database  pagemap;
	/**
	 * Opens the maps and store the current number of entries. Called once only.
	 * @throws IOException
	 */
	public Maps() throws IOException{
		wordmap=new IndexDatabase(jdbm.WORDMAP,jdbm.WORDMAP);
		pagemap=new IndexDatabase(jdbm.PAGEMAP,jdbm.PAGEMAP);
		if(!countedWord){countWord=wordmap.countEntry();countedWord=true;}
		if(!countedPage){countPage=pagemap.countEntry();countedPage=true;}
	}
	public Maps(String path) throws IOException{
		wordmap=new IndexDatabase(path+jdbm.WORDMAP,path+jdbm.WORDMAP);
		pagemap=new IndexDatabase(path+jdbm.PAGEMAP,path+jdbm.PAGEMAP);
		if(!countedWord){countWord=wordmap.countEntry();countedWord=true;}
		if(!countedPage){countPage=pagemap.countEntry();countedPage=true;}
	}
	/**
	 * Get the wordID with the word provided.
	 * @param word
	 * @return ID of the word provided.
	 * @throws IOException
	 */
	public static int getWordID(String word) throws IOException {
		int index;
		try{
			index=(Integer) wordmap.get(word);
		}catch(NullPointerException e){
			wordmap.add(word, countWord);
			index=countWord++;
		}
		return index;
	}
	/**
	 * Get the PageID with the url provided.
	 * @param url
	 * @return ID of the URL provided.
	 * @throws IOException
	 */
	public static int getPageID(String url) throws IOException {
		int index;
		try{
			index=(Integer) pagemap.get(url);
		}catch(NullPointerException e){
			pagemap.add(url, countPage);
			index=countPage++;
		}
		return index;
	}
	/**
	 * Close both databases.
	 */
	public void finalize() throws IOException{
		wordmap.finalize();
		pagemap.finalize();
	}
	/**
	 * Commit booth databases.
	 * @throws IOException
	 */
	public static void commit() throws IOException{
		wordmap.commit();
		pagemap.commit();
	}
}
