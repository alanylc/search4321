package projcore;


import java.io.IOException;

/**
 * Database storing pages, add function overriden.
 * Key:PageID
 * Data:Crawler Object
 * @author hin
 *
 */
public class PageDatabase extends Database {
/**
 * Construct or load database.
 * @param recordmanager Database name.
 * @param objectname Object name.
 * @throws IOException
 */
	public PageDatabase(String recordmanager, String objectname) throws IOException {
		super(recordmanager, objectname);
	}
	/**
	 * Add new entry to page database.
	 * @param id ID of page
	 * @param c crawled page.
	 * @throws IOException
	 */
	public void add(int id, Crawler c,boolean commit) throws IOException {
		hashtable.put(id,c);
	    if(commit)recman.commit();
	}
	/**
	 * Get the crawler with key provided
	 * @param i Integer key provided
	 * @return Crawler Found.
	 * @throws IOException
	 */
	public Crawler get(int i) throws IOException{
		return (Crawler) hashtable.get(i);
	}

	/**
	 * Remove an entry from a database
	 * @param pid ID of Page to be removed.
	 * @throws IOException
	 */
	public void delEntry(int pid) throws IOException {
		hashtable.remove(pid);
	}

}
