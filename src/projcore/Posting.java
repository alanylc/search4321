package projcore;



import java.io.Serializable;

/**
 * Posting is a serializable String integer pair which represents docid/word (Doc1/word) and frequency of a word
 * @author hin
 *
 */
class Posting implements Serializable {
	public String word;
	public int freq;
	Posting(String word, int freq) {
		this.word = word;
		this.freq = freq;
	}
	@Override
	public String toString() {
		return  word+" "+freq+" " ;
	}
	
}