package projcore;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class SearchEngine {
	private static StopStem stopStem = null;
	private static boolean debug = true;
	public static String path = "";
	
	static Maps maps = null;
	static PageDatabase pdb = null;
	static IndexDatabase idb = null;
	/*static {
		// init databases
		try {
			maps = new Maps(path);
			pdb=new PageDatabase(path+jdbm.PAGEDB,path+jdbm.PAGEDB);
			idb=new IndexDatabase(path+jdbm.WORDDB,path+jdbm.WORDDB);
		stopStem = new StopStem(path+"stopwords.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}*/
	
	private static boolean hasWord(Vector<String> list, String target) {
		for (Iterator<String> it = list.iterator(); it.hasNext(); ) {
			if (it.next().equals(target)) return true;
		}
		return false;
	}
	
	// check whether the page contains the phrase
	// phrases are unstemmed
	public static boolean pageContains(Crawler page, Vector<String> phrases) {
		Vector<String> words = new Vector<String>();
		for (Iterator<Posting> it = page.words.iterator(); it.hasNext(); ) {
			Posting p = it.next();
			words.add(p.word);
		}
		for (Iterator<String> it = phrases.iterator(); it.hasNext(); ) {
			String s[] = it.next().split(" ");
			for (int i=0; i<s.length; i++) {
				String stemmed = null;
				if (!stopStem.isStopWord(s[i])){
	        		stemmed = stopStem.stem(s[i]);
	        		if(stemmed.equals("") || stemmed==null || stemmed.equals(" ")) {
	        			continue;
	        		}
				}
				if (!hasWord(words, stemmed)) return false;
			}
		}
		return true;
	}
	
	private static Posting getPostingFromPage(String word, Crawler page) {
		for (Iterator<Posting> it = page.words.iterator(); it.hasNext(); ) {
			Posting p = it.next();
			if (p.word.equals(word)) return p;
		}
		return null;
	}
	
	public static Vector<SearchResult> search(String query) throws IOException {
		// get all keywords and phrases in query
		Vector<String> phrases = new Vector<String>();
		Vector<String> keywords = new Vector<String>();
		HashMap<String, Integer> words = new HashMap<String, Integer>();
		
		String tmp = "";
		query = query.trim();
		query = query.replaceAll("[ ]+", " ");
		boolean isPhrase = false;
		for (int i=0; i<query.length(); i++) {
			char c = query.charAt(i);
			if (c == ' ' && !isPhrase) {
				if (!tmp.equals("") && tmp!=null) keywords.add(tmp.trim());
				tmp = "";
			}
			else if (i==query.length()-1 && !isPhrase) {
				tmp = tmp.concat(Character.toString(c)).trim();
				if (!tmp.equals("") && tmp!=null) keywords.add(tmp);
				tmp = "";
			}
			else if (c == '"') {
				if (isPhrase) {
					if (debug) System.out.println("Phrase: " + tmp);
					tmp = tmp.trim();
					if (!tmp.equals("") && tmp!=null) phrases.add(tmp);
					tmp = "";
					isPhrase = false;
				}
				else {
					if (!tmp.equals("") && tmp!=null) {
						if (!tmp.equals(" ")) keywords.add(tmp);
						tmp = "";
					}
					isPhrase = true;
				}
			}
			else {
				tmp = tmp.concat(Character.toString(c));
			}
		}
		
		// get all distinct words in query
		for (Iterator<String> it = keywords.iterator(); it.hasNext(); ) {
			String word = it.next();
			Integer n = words.get(word);
			if (n == null) n = 1; else n++;
			words.put(word, n);
		}
		for (Iterator<String> it = phrases.iterator(); it.hasNext(); ) {
			String[] wordsInPhrase = it.next().split(" ");
			for (int i=0; i<wordsInPhrase.length; i++) {
				Integer n = words.get(wordsInPhrase[i]);
				if (n == null) n = 1; else n++;
				words.put(wordsInPhrase[i], n);
			}
		}
		
		// remove stop words and do stemming
		HashMap<String, Integer> stwords = new HashMap<String, Integer>();
		for (Iterator<String> it = words.keySet().iterator(); it.hasNext(); ) {
        	String word = it.next();
        	if (!stopStem.isStopWord(word)){
        		String tmpword = stopStem.stem(word);
        		if(!tmpword.equals("") && tmpword!=null && !tmpword.equals(" "))
        			stwords.put(tmpword, words.get(word));
        	}
        }
		
		// print all words and stemmed words
		if (debug) {
			System.out.println("==================================");
			System.out.println("Query words:");
			for (Iterator<String> it = words.keySet().iterator(); it.hasNext(); ) {
				String s = it.next();
				Integer n = words.get(s);
				System.out.println(s + " : " + n);
			}
			System.out.println("==================================");
			System.out.println("Stemmed query words:");
			for (Iterator<String> it = stwords.keySet().iterator(); it.hasNext(); ) {
				String s = it.next();
				Integer n = stwords.get(s);
				System.out.println(s + " : " + n);
			}
			System.out.println("==================================");
		}
		
		// get all related pages
		Vector<Integer> relatedPageIDs = new Vector<Integer>();
		for (Iterator<String> it = stwords.keySet().iterator(); it.hasNext(); ) {
			String word = it.next();
			Vector<Posting> postingList = idb.getPostingList(word);
			if (postingList == null) continue;
			for (Iterator<Posting> pit = postingList.iterator(); pit.hasNext(); ) {
				Posting p = pit.next();
				Integer pid = Integer.parseInt(p.word.substring(3));
				if (!relatedPageIDs.contains(pid) && pageContains(pdb.get(pid), phrases)) {
					relatedPageIDs.add(pid);
				}
			}
		}
		
		// compute query vector
		// info is store in stwords (Java HashMap)
		double mag_q = 0.0;
		for (Iterator<Integer> it = stwords.values().iterator(); it.hasNext(); ) {
			int v = it.next();
			mag_q += v * v;
		}
		if (mag_q==0.0) mag_q = 1.0;
		else mag_q = Math.sqrt(mag_q);
		if (debug) System.out.println("mag_q= " + mag_q);
		
		// compute scores of each related pages
		//int N = relatedPageIDs.size();
		int N = pdb.countEntry();
		HashMap<Integer, Double> pageScores = new HashMap<Integer, Double>();
		for (Iterator<Integer> it = relatedPageIDs.iterator(); it.hasNext(); ) {
			int pageID = it.next();
			Crawler page = pdb.get(pageID);
			int max_tf = 1;
			/*
			for (Iterator<Posting> pit = page.words.iterator(); pit.hasNext(); ) {
				Posting p = pit.next();
				if (p.freq > max_tf) max_tf = p.freq;
			}*/
			max_tf=page.words.firstElement().freq;
			double score = 0.0;
			// compute partial score of summation d dot q
			for (Iterator<String> wit = stwords.keySet().iterator(); wit.hasNext(); ) {
				String word = wit.next();
				int weight_qt = stwords.get(word);
				int word_tf = 0;
				Posting pp = getPostingFromPage(word, page);
				if (pp != null) {
					word_tf = pp.freq;
				}
				int word_df = (idb.getPostingList(word)==null) ? 0 : idb.getPostingList(word).size();
				double word_idf = 1.0;
				if (word_df!=0) word_idf = Math.log((double)N/word_df) / Math.log(2);
				double weight_wd = word_tf * word_idf / max_tf;
				score += weight_qt * weight_wd;
				//mag_d += weight_wd * weight_wd;
			}
			double mag_d = 0.0;
			// compute magnitude of document
			for (Iterator<Posting> dpit = page.words.iterator(); dpit.hasNext(); ) {
				Posting p = dpit.next();
				int word_df = (idb.getPostingList(p.word)==null) ? 0 : idb.getPostingList(p.word).size();
				double word_idf = 1.0;
				if (word_df!=0) word_idf = Math.log(N/word_df) / Math.log(2);
				double weight_wd = p.freq * word_idf / max_tf;
				mag_d += weight_wd * weight_wd;
			}
			if (mag_d == 0.0) mag_d = 1.0;
			else mag_d = Math.sqrt(mag_d);
			score /= (mag_q * mag_d);
			
			// handle favor title
			Vector<String> stem_title_words = new Vector<String>();
			String[] title_words = page.title.split("[ ]+");
			for (int i=0; i<title_words.length; i++) {
				if (!stopStem.isStopWord(title_words[i])){
	        		String tmpword = stopStem.stem(title_words[i]);
	        		if(!tmpword.equals("") && tmpword!=null && !tmpword.equals(" ")) {
	        			if (!stem_title_words.contains(tmpword)) 
	        				stem_title_words.add(tmpword);
	        		}
				}
			}
			int query_distinct_word_count = stwords.size();
			int title_contains_count = 0;
			for (Iterator<String> sit = stwords.keySet().iterator(); sit.hasNext(); ) {
				String swd = sit.next().toLowerCase();
				for (Iterator<String> ssit = stem_title_words.iterator(); ssit.hasNext(); ) {
					String ssw = ssit.next().toLowerCase();
					if (ssw.equals(swd)) {
						title_contains_count++;
						break;
					}
				}
			}
			int unstem_word_count = 0;
			int distinct_unstem_query_word_count = words.size();
			for (Iterator<String> wdit = words.keySet().iterator(); wdit.hasNext(); ) {
				String wd = wdit.next().toLowerCase();
				for (int i=0; i<title_words.length; i++) {
					if (wd.equals(title_words[i].toLowerCase())) {
						unstem_word_count++;
						break;
					}
				}
			}
			double boostRate = 1 + (2 * title_contains_count / query_distinct_word_count) + (1 * unstem_word_count / distinct_unstem_query_word_count);
			score *= boostRate;
			if (score == 0.0) {
				score = boostRate - 1;
			}
			if (score != 0.0) pageScores.put(pageID, score);
		}
		
		pageScores = (HashMap<Integer, Double>) sortByValue(pageScores, true);
		
		if (debug) {
			System.out.println("==================================");
			for (Iterator<Integer> it = pageScores.keySet().iterator(); it.hasNext(); ) {
				Integer pid = it.next();
				double score = pageScores.get(pid);
				System.out.println(pid + " : " + score + "  (" + pdb.get(pid).url + ") [" + pdb.get(pid).title + "]");
			}
			System.out.println("==================================");
		}
		
		// return result of search
		Vector<SearchResult> result = new Vector<SearchResult>();
		for (Iterator<Integer> it = pageScores.keySet().iterator(); it.hasNext(); ) {
			SearchResult res = new SearchResult();
			Integer pid = it.next();
			Crawler page = pdb.get(pid);
			res.score = pageScores.get(pid);
			res.title = page.title;
			res.url = page.url;
			res.lastMod = page.lastMod;
			res.size = page.size;
			Vector<Posting> keywd = new Vector<Posting>();
			/*for (Iterator<String> sit = stwords.keySet().iterator(); sit.hasNext(); ) {
				String sword = sit.next();
				Posting posting = getPostingFromPage(sword, page);
				int freq = (posting == null) ? 0 : posting.freq;
				keywd.put(sword, freq);
			}*/
			for (Iterator<Posting> ptit = page.words.iterator(); keywd.size()<5 && ptit.hasNext(); ) {
				Posting p = ptit.next();
				keywd.add(p);
			}
			res.keywords = new Vector<Posting>( keywd );
			res.parentLink = new Vector<String>( page.parentLink );
			res.childLink = new Vector<String>( page.childLink );
			
			if (pageContains(page, phrases)) result.add(res);
		}
		return result;
	}
	
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map , final boolean desc) {
	    List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>( map.entrySet() );
	    Collections.sort( list, new Comparator<Map.Entry<K, V>>() {
	        public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 ) {
	            int ret = (o1.getValue()).compareTo( o2.getValue() );
	            if (desc) ret = -ret;
	            return ret;
	        }
	    });
	
	    Map<K, V> result = new LinkedHashMap<K, V>();
	    for (Map.Entry<K, V> entry : list)
	    {
	        result.put( entry.getKey(), entry.getValue() );
	    }
	    return result;
	}
	
	public static String doSearch(String query) {
		try {
			StringBuilder str = new StringBuilder();
			Vector<SearchResult> res = search(query);
			close();
			str.append("<table border='1'>\n");
			int count = 0;
			for (Iterator<SearchResult> it = res.iterator(); count<50 && it.hasNext(); ) {
				SearchResult r = it.next();
				str.append(r.toHTMLTableRow()+"\n");
				count++;
			}
			if (res.size()==0) str.append("<p>No Results</p>");
			str.append("</table>\n");
			return str.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static void setPath(String _path) {
		path = _path;
		try {
			maps = new Maps(path);
			pdb=new PageDatabase(path+jdbm.PAGEDB,path+jdbm.PAGEDB);
			idb=new IndexDatabase(path+jdbm.WORDDB,path+jdbm.WORDDB);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void setup(Maps m, PageDatabase p, IndexDatabase w, StopStem s) {
		maps = m;
		pdb = p;
		idb = w;
		stopStem = s;
		System.out.println(">> setup");
	}

	public static void close(){
		try {
			maps.finalize();
			pdb.finalize();
			idb.finalize();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void init() {
		try {
			maps = new Maps(path);
			pdb=new PageDatabase(path+jdbm.PAGEDB,path+jdbm.PAGEDB);
			idb=new IndexDatabase(path+jdbm.WORDDB,path+jdbm.WORDDB);
			stopStem = new StopStem(path+"stopwords.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		init();
		String str;
		str = doSearch("test page");
		//str = doSearch("employ \"info employ\" 3");
		//str = doSearch("year 3 \"comp scien\" yu");
		//str = doSearch("News year yu pg ug \"computer engineering\" year comput yu 2");
		//str = doSearch("News year yu pg ug\"yu\" year comput yu 2");
		//str = doSearch("Beng comp scien");
		//str = doSearch("information for employers");
		System.out.println(str);
	}
}
