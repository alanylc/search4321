package projcore;


import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

public class SearchResult {
	public double score;
	public String title;
	public String url;
	public Date lastMod;
	public int size;
	public Vector<Posting> keywords;
	public Vector<String> parentLink;
	public Vector<String> childLink;
	
	public String toHTMLTableRow() {
		StringBuilder html = new StringBuilder();
		html.append("<tr>\n <td valign='top'>" + score + "</td>\n <td>");
		html.append("\n <p>Title: " + title + "</p>\n <p>URL: <a href='" + url + "'>" + url + "</a></p>\n <p>Date: ");
		html.append(lastMod.toString() + "</p>\n <p>Size: " + size + "</p>\n <p>");
		for (Iterator<Posting> it = keywords.iterator(); it.hasNext(); ) {
			Posting p = it.next();
			int freq = p.freq;
			String kw = p.word;
			html.append(kw + " (" + freq + ")");
			if (it.hasNext()) html.append(", ");
		}
		html.append("</p>\n <span>Parent Link:</span>");
		for (Iterator<String> it = parentLink.iterator(); it.hasNext(); ) {
			String link = it.next();
			html.append("\n <p><a href='" + link + "'>" + link + "</a></p>");
		}
		if (parentLink.size()==0) html.append("\n <p>None</p>");
		html.append("\n <span>Child Link:</span>");
		for (Iterator<String> it = childLink.iterator(); it.hasNext(); ) {
			String link = it.next();
			html.append("\n <p><a href='" + link + "'>" + link + "</a></p>");
		}
		if (childLink.size()==0) html.append("\n <p>None</p>");
		html.append("\n </td></tr>");
		return html.toString();
	}
}
