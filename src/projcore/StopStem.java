package projcore;



import java.io.*;


public class StopStem
{
	private Porter porter;
	private java.util.HashSet stopWords;
	public boolean isStopWord(String str)
	{
		return stopWords.contains(str);	
	}
	public StopStem(String str)
	{
		super();
		porter = new Porter();
		stopWords = new java.util.HashSet();
		try {

			BufferedReader br = new BufferedReader(new FileReader(str));
			String line;
			while ((line = br.readLine()) != null) {
			   // process the line.

				stopWords.add(line);
			}
			br.close();
		}catch (FileNotFoundException e) {
			System.out.println("Not found");
		}catch(IOException e){
			System.out.println("Error when opening.");
		}

		
	}
	public String stem(String str)
	{
		return porter.stripAffixes(str);
	}
	public static void main()
	{
		StopStem stopStem = new StopStem("stopwords.txt");
		String input="";
		try{
			do
			{
				System.out.print("Please enter a single English word: ");
				BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
				input = in.readLine();
				if(input.length()>0)
				{	
					if (stopStem.isStopWord(input))
						System.out.println("It should be stopped");
					else
			   			System.out.println("The stem of it is \"" + stopStem.stem(input)+"\"");
				}
			}
			while(input.length()>0);
		}
		catch(IOException ioe)
		{
			System.err.println(ioe.toString());
		}
	}
}