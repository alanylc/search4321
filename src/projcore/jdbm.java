package projcore;


import java.io.IOException;
import java.nio.file.Paths;




/*
 * RecordManager can be interpreted as a manager of Database, it responses for the communication with the hard disk, 
 * under which the objects created will not be lost after powering off. The content stored in recman is object, including
 * hashtable. Hashtable is only one implementation of indexing (others like Btree), it can store the records into memory but cannot write them into hard disk,
 * and it needs the help of record manager. That is why hash table has to be provided with an argument of record manager
 * during the creation. After being created, hash table will automatically be stored in the corresponding record manager.  */

public class jdbm {
	private static final String prefix = ""; 
	//private static final String prefix = "/comp4321/ylchungaa/public_html/";
	//private static final String prefix = Paths.get("").toAbsolutePath().toString();
	/*
	public static final String DATABASE = Paths.get("db").toString();
	public static final String PAGEDB = Paths.get("pagedb").toString();
	public static final String WORDDB = Paths.get("worddb").toString();
	public static final String WORDMAP = Paths.get("wordmap").toString();
	public static final String PAGEMAP = Paths.get("pagemap").toString();
	*/
	public static final String DATABASE = prefix+"db";
	public static final String PAGEDB = prefix+"pagedb";
	public static final String WORDDB = prefix+"worddb";
	public static final String WORDMAP = prefix+"wordmap";
	public static final String PAGEMAP = prefix+"pagemap";
	
	public static void main(String[] args) {
		//StopStem.main();
		try {
			Maps m=new Maps();
			final int ttl = 300;
			if(args.length!=0)Crawler.main(args[0],args[1],Integer.parseInt(args[2]));
				else Crawler.main("http://www.cse.ust.hk/~ericzhao/COMP4321/TestPages/testpage.htm","http://www.cse.ust.hk/~ericzhao/COMP4321/TestPages", ttl);
			PageDatabase pdb=new PageDatabase(jdbm.PAGEDB,jdbm.PAGEDB);
			IndexDatabase idb=new IndexDatabase(jdbm.WORDDB,jdbm.WORDDB);
			Database wmdb=new Database(jdbm.WORDMAP,jdbm.WORDMAP);
			Database pmdb=new Database(jdbm.PAGEMAP,jdbm.PAGEMAP);
			m.finalize();
			pdb.iprintAll(false);
			idb.iprintAll(false);
			wmdb.printAll();
			pmdb.printAll();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
